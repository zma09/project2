/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zm.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Aphasia
 */
//这是业务逻辑处理页面model类与goods表相关的业务逻辑
public class GoodsBeanBO {

    //定义一些变量
    private ResultSet rs = null;
    private Connection conn = null;
    private PreparedStatement ps = null;
    
    //返回多少页
    public int getPageCount(int pageSize){
        int pageCount=0;
        int rowCount=0;
        try {
            conn=new ConnDB().getConn();
            ps = conn.prepareStatement("select count(*) from goods");
            //共有多少条记录
            rs=ps.executeQuery();
            if (rs.next()) {
                
                rowCount=rs.getInt(1);
                
            }
            if (rowCount%pageSize==0) {
                pageCount=rowCount/pageSize;
            }else{
                pageCount=rowCount/pageSize+1;
            }
        } catch (Exception e) {
            
            e.getStackTrace();
            
        }finally{
            this.close();
        } 
        return pageCount;
    }
    
//分页显示商品货物的信息
    public ArrayList getGoodsByPage(int pageSize,int pageNow){
        ArrayList al =new ArrayList();
        try {
            conn=new ConnDB().getConn();
            ps = conn.prepareStatement("select * from goods limit "+pageSize*(pageNow-1)+","+pageSize+";");
            rs=ps.executeQuery();
            while (rs.next()) { 
                GoodsBean gb=new GoodsBean();
                gb.setGoodsId(rs.getInt(1));
                gb.setGoodsName(rs.getString(2));
                gb.setGoodsIntro(rs.getString(3));
                gb.setGoodsPrice(rs.getFloat(4));
                gb.setGoodsNum(rs.getInt(5));
                gb.setPublisher(rs.getString(6));
                gb.setPhoto(rs.getString(7));
                gb.setGoodsType(rs.getString(8));
                
                //加入到al里面
                al.add(gb);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            this.close();
        }
        return al;
    }
    
    //根据一个货物ID得到商品货物的具体信息的函数
    public GoodsBean getGoodsBean(String id) {
        GoodsBean gb = new GoodsBean();

        try {

            conn = new ConnDB().getConn();
            ps = conn.prepareStatement("select * from goods where goodsId=?");
            ps.setString(1, id);
            rs = ps.executeQuery();

            if (rs.next()) {

                //放入gb
                gb.setGoodsId(rs.getInt(1));
                gb.setGoodsName(rs.getString(2));
                gb.setGoodsIntro(rs.getString(3));
                gb.setGoodsPrice(rs.getFloat(4));
                gb.setGoodsNum(rs.getInt(5));
                gb.setPublisher(rs.getString(6));
                gb.setPhoto(rs.getString(7));
                gb.setGoodsType(rs.getString(8));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            //关闭资源！！！！十分重要！！！！！！
            this.close();
        }
        return gb;
    }
    //关闭资源函数
    public  void close(){
        try {
            if(rs!=null){
                rs.close();
                rs=null;//把它当做垃圾，让系统回收
            }
            
            if(ps!=null){
                ps.close();
                ps=null;//把它当做垃圾，让系统回收
            }
            
            if(!conn.isClosed()){
                conn.close();
               // conn=null;//把它当做垃圾，让系统回收
            }
        } catch (Exception e) {
        
            e.printStackTrace();
        }
    }
}
