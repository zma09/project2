/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//处理类，有些人喜欢把它叫做BO，主要是封users表的各种操作（只要是增，删，改，查）
//他的一个实例（对象），对用users表的一个记录
package com.zm.model;
import java.util.*;
import java.sql.*;

/**
 *
 * @author Aphasia
 */
public class UserBeanCl {
       private Statement stmt = null;
       private ResultSet rs = null;
       private Connection conn = null;
       private int pageSize=3;
       private int rowCount = 0;//该值从数据库查询
       private int pageCount = 0;//该值通过pageSize和rowCount
       private int idNum =0;
       //添加用户
       public boolean addUser(String name,String firstname,String lastname,String passwd,String email,String phone,String address,String grade){
             boolean b=false ;
           
           try {
               //的得到连接
               conn= new ConnDB().getConn();
               stmt=conn.createStatement();
              rs =stmt.executeQuery("select count(*) from users ");
              if(rs.next()){//一定要写rs.next()不写的话并没有指到第一条会指到第一条之前一条，所以一定要写
                    rowCount=rs.getInt(1);
                    System.out.println(rowCount);
                 }
               //执行
              idNum=rowCount+1;
              //idNum=Integer.valueOf().intValue()
               System.out.println(idNum);
             System.out.println("insert into users(username,firstname,lastname, passwd, email,phone,address, grade) values('"+name+"','"+firstname+"','"+lastname+"','"+passwd+"','"+email+"','"+phone+"','"+address+"','"+grade+"')");
              int a=stmt.executeUpdate("insert into users(username,firstname,lastname, passwd, email,phone,address, grade) values('"+name+"','"+firstname+"','"+lastname+"','"+passwd+"','"+email+"','"+phone+"','"+address+"','"+grade+"')");
             //int a=stmt.executeUpdate("insert into users values('"+idNum+"','"+name+"','"+passwd+"','"+email+"','"+grade+"')");
              
               if(a==1){
                   //添加成功
                   b=true;
               }
               
           } catch (Exception e) {
               e.printStackTrace();
           }finally{
               this.close();
           }
           return b;
       }
       //删除用户
       public boolean delUserById(String id){
           boolean b=false ;
           System.out.println(id);
           try {
               //的得到连接
               conn= new ConnDB().getConn();
               stmt=conn.createStatement();
               //执行
              int a=stmt.executeUpdate("delete from users where userId  = '"+id+"'");
              System.out.println("delete from users where userId  = '"+id+"'");
               if(a==1){
                   //删除成功
                   b=true;
               }
               
           } catch (Exception e) {
               e.printStackTrace();
           }finally{
               this.close();
           }
           return b;
       }
       public boolean updataUser(String id,String name,String firstname,String lastname,String passwd,String email,String phone,String address,String grade){
             boolean b=false ;
           
           try {
               //的得到连接
               conn= new ConnDB().getConn();
               stmt=conn.createStatement();
              int a=stmt.executeUpdate("update users SET username='"+name+"',firstname='"+firstname+"',lastname='"+lastname+"',passwd='"+passwd+"',email='"+email+"',phone='"+phone+"',address='"+address+"',grade='"+grade+"' where userId='"+id+"'");
             //int a=stmt.executeUpdate("insert into users values('"+idNum+"','"+name+"','"+passwd+"','"+email+"','"+grade+"')");
              
               if(a==1){
                   //添加成功
                   b=true;
               }
               
           } catch (Exception e) {
               e.printStackTrace();
           }finally{
               this.close();
           }
           return b;
       }
       //返回分页的总页数
        public int getPageCount(){
            
            try {
                //得到连接
                conn= new ConnDB().getConn();
                stmt=conn.createStatement();
                rs =stmt.executeQuery("select count(*) from users ");//显示表里面一共有多少条记录
                //rs = stmt.getResultSet();
                if(rs.next()){//一定要写rs.next()不写的话并没有指到第一条会指到第一条之前一条，所以一定要写
                    rowCount=rs.getInt(1);
                    
                 }
                //计算pageCount 算法较多可以自己设计
                 if(rowCount%pageSize == 0){
                       pageCount = rowCount / pageSize;
                  }else{
                       pageCount = rowCount / pageSize + 1;//算法较多
                 }
            } catch (Exception e) {
                e.printStackTrace();
            }finally{
                this.close();
            }
            return pageCount;
        }

        //得到用户需要显示的用户信息（分页）
       public ArrayList getUsersByPage(int pageNow){
       
           ArrayList al = new ArrayList();
           try {
               conn= new ConnDB().getConn();
               //3.创建Statement
               stmt = conn.createStatement();
               //查询出需要显示的记录
               rs = stmt.executeQuery("select * from users limit "+pageSize*(pageNow-1)+","+pageSize+";");
                 //开始将rs封装到ArrayList
                while (rs.next()) {
                   UserBean ub = new UserBean();
                   ub.setUserid(rs.getInt(1));
                   ub.setUsername(rs.getString(2));
                   ub.setFirstname(rs.getString(3));
                   ub.setLastname(rs.getString(4));
                   ub.setPasswd(rs.getString(5));
                   ub.setEmail(rs.getString(6));
                   ub.setPhone(rs.getString(7));
                   ub.setAddress(rs.getString(8));
                   ub.setGrade(rs.getInt(9));
                   //将ub放入到arraylist中
                   al.add(ub);
               }
           } catch (Exception e) {
               e.printStackTrace();
           }finally{
               this.close();
           }
           return al;
       }
               
    //关闭资源函数
    public void close(){
        try {
                if (rs!=null) {
                    rs.close();
                    rs=null;
                }if (stmt!=null) {
                    stmt.close();
                    stmt=null;
                }if (conn!=null) {
                    conn.close();
                    conn=null;
                }
            } catch (Exception e) {
            }
    }
    //验证用户是否存在
    public boolean checkUser(String u,String p){
        boolean b=false;
        
        try {
            //到数据库去验证用户
            conn = new ConnDB().getConn();
             //3.创建Statement
                        stmt = conn.createStatement();
              //4.查询
                        stmt.execute("select passwd from users where username='"+u+"'");
                        rs = stmt.getResultSet();
              //根据结果作判断
                        if(rs.next()){
                            //说明用名户存在
                            if(rs.getString("passwd").equals(p)){
                               //一定是合法的
                                b=true;
                            }else{
                                //密码错了
                               b=false;
                             }
                            
                        }else{
                        //说明用户名都错了
                            b=false;
                        }     
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally{
           //关闭打开的各种资源 ，这个很重要
           /* try {
                if (rs!=null) {
                    rs.close();
                    rs=null;
                }if (stmt!=null) {
                    stmt.close();
                    stmt=null;
                }if (conn!=null) {
                    conn.close();
                    conn=null;
                }
            } catch (Exception e) {
            }*/
            //封装成一个函数
            this.close();
        }
     return b;
    
    }
   
}

