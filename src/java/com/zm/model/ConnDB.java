/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.zm.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author Aphasia
 */
//这是一个model类，完成得到一个数据库连接的
public class ConnDB {
//     private Connection conn= null;
//    
//    public Connection getConn(){
//        try{
//            Class.forName( "com.mysql.jdbc.Driver" ) ;
//	    String sURL="jdbc:mysql://127.0.0.1:3306/shopping";
//	    String sUserName="root";
//	    String sPwd="";
//            conn = DriverManager.getConnection(sURL,sUserName,sPwd);
//	    Statement stmt = conn.createStatement();
//            ResultSet rs = stmt.getResultSet(); // get any ResultSet that came from our query
//        } catch(Exception e){
//            //一定写上，不写错了不知道怎么调试
//            e.printStackTrace();
//        
//        }
//        return conn;
//    
//    }
    private DataSource datasource;
    
    // Add in constructor that takes in the source of database connections and store it
    public ConnDB(DataSource myDataSource)
    {
        this.datasource = myDataSource;
    }
    
    // Constructor for use by pooled connections
    public ConnDB()
    {
        Connection conn = null;
        String DATASOURCE_CONTEXT = "jdbc/shopping";
        try {
            Context initialContext = new InitialContext();
            DataSource ds = (DataSource)initialContext.lookup("java:comp/env/" + DATASOURCE_CONTEXT);
            if(ds != null){
                datasource = ds;
            }
            else{
		System.out.println(("Failed to lookup datasource."));
            }
        }catch (NamingException e ){
            System.out.println("Cannot get connection: " + e);
        }
    }
    
   
    public Connection getConn() 
    {
        Connection conn = null;
        try{
            if (datasource != null) {
                conn = datasource.getConnection();
               Statement stmt = conn.createStatement();
                ResultSet rs = stmt.getResultSet();
            }else {
                System.out.println(("Failed to lookup datasource."));
            }
        }catch (SQLException ex2){
            System.out.println("Connection failed " + ex2.getMessage());
            System.exit(2); // Abnormal termination
        }
        return conn;
    }

    public void freeConnection(Connection conn)  
    {
        try 
        {
            if (conn != null) 
            {
                conn.close();
                conn = null;
            }
        } 
        catch (SQLException e) 
        {
            System.out.println("Failed to free connection: " + e.getMessage());
            System.exit(1);
        }
    }
//    private Connection ct=null;
//    public Connection getConn(){
//		try{
//		Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
//	    ct=DriverManager.getConnection("jdbc:odbc:testdb2");
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//	    return ct;
//	}
}
