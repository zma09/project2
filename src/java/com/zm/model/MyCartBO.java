/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//这是一个bo（model）
//用于处理购物相关的购物逻辑
package com.zm.model;
import java.sql.*;
import java.util.*;
/**
 *
 * @author Aphasia
 */
public class MyCartBO {
    private PreparedStatement ps = null;
	private ResultSet rs = null;
	private Connection conn = null;
    //定义一个haspmap集合，用于存放id和书的数量
    HashMap <String,String>hm=new HashMap<String,String>();
    private float allPrice=0.0f;
    public float getAllPrice(){
		return this.allPrice;
	}
	
	//根据id返回货物的数量
	public String getGoodsNumById(String goodsId){
		return (String)hm.get(goodsId);
	}
    //1.添加货物
    public void addGoods(String goodsId,String goodsNum){
        hm.put(goodsId, goodsNum);
    }
    //2.删除货物
    public void delGoods(String goodsId){
        hm.remove(goodsId);
    }
    //3.清空货物
    public void clear(){
		hm.clear();
	}
    //4.修改货物数量
    public void upGoods(String goodsId,String newNum){
		hm.put(goodsId, newNum);
	}
    //5.显示购物车
    public ArrayList showMyCart(){
		ArrayList <GoodsBean> al=new ArrayList<GoodsBean>();			//泛型功能
		try{
			//组织sql语句能够
			String sql="select * from goods where goodsId in";//(5,6)
			//将会使用迭代器完成从hm中取出货物id的要求
			Iterator it=hm.keySet().iterator();
			
			String sub="(";
			while(it.hasNext()){
				//取出GoodsId
				String goodsId=(String)it.next();
				//判断是不是最后id
				if(it.hasNext()){
				sub+=goodsId+",";
				}else{
					sub+=goodsId+")";
				}
                                //test2
			//1,5
                          //   sub="(1,5)";
                        }
			sql+=sub;
			conn=new ConnDB().getConn();
			ps=conn.prepareStatement(sql);
			rs=ps.executeQuery();
                        this.allPrice=0.0f;
			while(rs.next()){
				GoodsBean gb=new GoodsBean();
                                int goodsId=rs.getInt(1);
				gb.setGoodsId(goodsId);
				gb.setGoodsName(rs.getString(2));
				gb.setGoodsIntro(rs.getString(3));
                                float unit=rs.getFloat(4);
                                gb.setGoodsPrice(unit);
				gb.setGoodsPrice(rs.getFloat(4));
				gb.setGoodsNum(rs.getInt(5));
				gb.setPublisher(rs.getString(6));
				gb.setPhoto(rs.getString(7));
				gb.setGoodsType(rs.getString(8));
				//加入al
                                this.allPrice=this.allPrice+unit*Integer.parseInt(this.getGoodsNumById(goodsId+""));
				al.add(gb);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			this.close();
		}
		return al;
	}
	
	public void close() { // 关闭各种打开的资源
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}
			if (ps != null) {
				ps.close();
				ps = null;
			}
			if (conn != null) {
				conn.close();
				conn = null;
			}
		} catch (Exception e) {
			e.printStackTrace();// 打印异常，以便修改
		}
	}
        //test
//        public static void main (String [] args){
//        HashMap hm2=new HashMap();
//        hm2.put("1", "3");
//        hm2.put("2", "4");
//        hm2.put("5", "100");
//        //取出所有key
//        //hm2.remove("2");
//      //hm2.clear();
//        //使用迭代器
//        Iterator it=hm2.keySet().iterator();
//        
//            while (it.hasNext()) {
//                //取出key
//                String key=(String)it.next();
//                System.out.println("key===="+key);
//                //根据key 取出value
//                String nums2=(String)hm2.get(key);
//                System.out.println("key===="+key+" num======"+nums2);
//            }
//        
//        }


}


