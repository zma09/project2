/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.zm.servlet;

import com.zm.model.UserBeanCl;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
/**
 *
 * @author Aphasia
 */
@WebServlet(name = "LoginClServlet", urlPatterns = {"/LoginClServlet"})
public class LoginClServlet extends HttpServlet {

 

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
         //得到用户名和密码
        String u=request.getParameter("username");
        String p=request.getParameter("password");
        //使用模型（UserBeanCl），完成对用户的验证
        //1.创建一个UserBeanCl对象
        UserBeanCl ubc=new UserBeanCl();
        //调用方法
        if (ubc.checkUser(u, p)) {
            System.out.println("这次是使用了Servlet控制器完成验证");
            //在跳转到wel.jsp页面时就把要显示的数据装备好
            ArrayList al=ubc.getUsersByPage(1);
            int pageCount=ubc.getPageCount();
            //将al,pageCount,pageNow放入request中
            request.setAttribute("result", al);
            request.setAttribute("pageCount", pageCount+"");
            request.setAttribute("pageNow", "1");
            //合法
            //转向,因为sendRedirect方法效率不高，所以软件公司常常是用转发的方法
            //response.sendRedirect("wel.jsp");
            
            //将用户名放入session,以备后用
            request.getSession().setAttribute("myName", u);
            
            //这种方法效率高同时request response中的对象还可以在下一个页面使用
            request.getRequestDispatcher("main.jsp").forward(request,response);
            
        }else{
            //不合法
            //response.sendRedirect("login.jsp");
            request.getRequestDispatcher("login.jsp").forward(request,response);
        }
    }

  

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

    
    

}
