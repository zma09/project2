/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//这个控制器，将处理用户的分页显示，删除，添加，修改
package com.zm.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.zm.model.*;
import java.util.ArrayList;
/**
 *
 * @author Aphasia
 */
public class UsersClServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       
       
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //获得标识值
        String flag=request.getParameter("flag");
        //如果是要分页显示用户信息
        if (flag.equals("fengye")) {
            
        
        
        //得到用户希望显示的pageNow
        String s_pageanow = request.getParameter("pageNow");
        try {
            
            System.out.println("分页使用的userclservlet成功");
            int pageNow=Integer.parseInt(s_pageanow);
            //调用userBeanCl
           UserBeanCl ubc =new UserBeanCl();
            ArrayList al=ubc.getUsersByPage(pageNow);
            int pageCount=ubc.getPageCount();
            //将al,pageCount,pageNow放入request中
            request.setAttribute("result", al);
            request.setAttribute("pageCount", pageCount+"");
            request.setAttribute("pageNow", pageNow+"");
            //重新跳转回wel.jsp
            request.getRequestDispatcher("wel.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
            }
        }else if(flag.equals("delUser")){
            //完成删除用户
            //得到要删除用户的ID号
           String userId=request.getParameter("userId");
            //创建一个UserBeanCl
            UserBeanCl ubc=new UserBeanCl();
            
           // boolean b=ubc.delUserById("userId");
            if(ubc.delUserById(userId)){
                //删除成功
                request.getRequestDispatcher("suc.jsp").forward(request, response);
                System.out.println("调试成功");
            }else{
                //删除不成功
                request.getRequestDispatcher("err.jsp").forward(request, response);
                System.out.println("不成功");
            }
        }else if(flag.equals("addUser")){
            //完成添加用户
            //得到用户输入的信息
      
            String name=request.getParameter("userName");
            String firstname=request.getParameter("firstName");
            String lastname=request.getParameter("lastName");
            String passwd=request.getParameter("passwd");
            String email=request.getParameter("email");
            String phone=request.getParameter("phone");
            String address=request.getParameter("address");
            String grade=request.getParameter("grade");
            
            //创建一个UserBeanCl
            UserBeanCl ubc=new UserBeanCl();
            
           // boolean b=ubc.delUserById("userId");
            if(ubc.addUser(name, firstname, lastname, passwd, email, phone, address, grade)){
                //添加成功
                request.getRequestDispatcher("suc.jsp").forward(request, response);
                System.out.println("调试成功");
            }else{
                //删除不成功
                request.getRequestDispatcher("err.jsp").forward(request, response);
                System.out.println("不成功");
            }
        }else if(flag.equals("updataUser")){
            //完成添加用户
            //得到用户输入的信息
            String id=request.getParameter("userId");
            String name=request.getParameter("userName");
            String firstname=request.getParameter("firstName");
            String lastname=request.getParameter("lastName");
            String passwd=request.getParameter("passwd");
            String email=request.getParameter("email");
            String phone=request.getParameter("phone");
            String address=request.getParameter("address");
            String grade=request.getParameter("grade");
            
            //创建一个UserBeanCl
            UserBeanCl ubc=new UserBeanCl();
            
           // boolean b=ubc.delUserById("userId");
            if(ubc.updataUser(id, name, firstname, lastname, passwd, email, phone, address, grade)){
                //添加成功
                request.getRequestDispatcher("suc.jsp").forward(request, response);
                System.out.println("调试成功");
            }else{
                //删除不成功
                request.getRequestDispatcher("err.jsp").forward(request, response);
                System.out.println("不成功");
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.doGet(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
