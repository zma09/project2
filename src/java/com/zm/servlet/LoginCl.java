/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.zm.servlet;

import com.zm.model.UserBeanBO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.zm.model.*;
import java.util.ArrayList;
import org.apache.log4j.Logger;
/**
 *
 * @author Aphasia
 */
public class LoginCl extends HttpServlet {

    static Logger log = Logger.getLogger("global");
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
       //得到用户名和密码，验证用户是否合法
        String u=request.getParameter("username");
        
        String p=request.getParameter("password");
        //验证用户
        UserBeanBO ubb=new UserBeanBO();
        if (ubb.checkUser(u, p)) {
            //用户合法
            //1.把成功登陆用户信息放入session[后面用到]
            UserBean ub=ubb.getUserBean(u);
            request.getSession().setAttribute("userInfo", ub);
            //2.把购物车的信息取出，准备下一个页面的显示
            MyCartBO mcb =(MyCartBO)request.getSession().getAttribute("mycart");
            ArrayList al =mcb.showMyCart();
            
            //把al 放入request 为了节省内存
            request.setAttribute("mycartInfo", al);
            
            request.getRequestDispatcher("shopping3.jsp").forward(request, response);
        }else{
            //用户不合法
            request.getRequestDispatcher("shopping2.jsp").forward(request, response);
        }
        
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        this.doGet(request, response);
    }

    

}
