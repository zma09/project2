DROP PROCEDURE IF EXISTS discounted;
DELIMITER //
CREATE PROCEDURE discounted
(
	IN customerID INTEGER, IN product INTEGER,
	IN qty INTEGER, IN discount DECIMAL(5,2)
)
BEGIN
        DECLARE unitprice, originalprice, totalprice DECIMAL(5,2);
	SELECT price FROM products WHERE id=product INTO unitprice; 
	SET originalprice = qty*unitprice;
	SET totalprice = originalprice - (originalprice*discount/100.0);
	INSERT INTO billitems (supplied, customerID, product, qty, amount)
	VALUES (CURRENT_DATE(), customerID, product, qty, totalprice); 
END //
DELIMITER ;

drop procedure if exists addMember;
create procedure addMember(username varchar(30),firstname varchar(30),lastname varchar(30),passwd varchar(30),email varchar(40),phone varchar(40),address varchar(100),grade int)
insert into users(username,firstname,lastname,passwd,email,phone,address,grade) values (username,firstname,lastname,passwd,email,phone,address,grade);


DROP TRIGGER IF EXISTS t_afterinsert_on_uders;
DELIMITER //
CREATE TRIGGER t_afterinsert_on_uders
 AFTER INSERT ON users
 FOR EACH ROW
BEGIN
insert into newusers(username,firstname,lastname,passwd,email,phone,address,grade) values(new.username,new.firstname,new.lastname,new.passwd,new.email,new.phone,new.address,new.grade);
 END;

DROP TRIGGER /*!50032 IF EXISTS */ tr_user_update
DELIMITER //
CREATE
    /*!50017 DEFINER = 'root'@'localhost' */
    TRIGGER tr_user_update AFTER UPDATE ON users
    FOR EACH ROW BEGIN 
		IF  old.username != new.username THEN
			UPDATE newusers SET username=NEW.username,firstname=NEW.firstname,lastname=NEW.lastname,passwd=NEW.passwd,email=NEW.email,phone=NEW.phone,address=NEW.address,grade=NEW.grade;
		END IF;
          
    END;

DROP TRIGGER IF EXISTS tr_user_delete;
DELIMITER //
CREATE /*!50017 DEFINER = 'root'@'localhost' */
    TRIGGER tr_user_delete AFTER DELETE ON users 
    FOR EACH ROW BEGIN
       if grade > 2 then
        DELETE FROM newusers WHERE userId=Old.userId;
    end if;
END;

start transaction;
SET AUTOCOMMIT=0;
insert into users(username,firstname,lastname,passwd,email,phone,address,grade) values (username,firstname,lastname,passwd,email,phone,address,grade);
COMMIT;
ROLLBACK;