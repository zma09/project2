
create table if not exists users
(
    userid int not null AUTO_INCREMENT,
    username varchar(30) not null,
    firstname varchar(30) not null,
    lastname varchar(30) not null,
    passwd varchar(30) not null,
    email varchar(40) not null,
    phone varchar(40) not null,
    address varchar(100) not null,
    grade int not null,
    PRIMARY KEY (userId)
);

create table goods
(
    goodsId int not null AUTO_INCREMENT,
    goodsName varchar(40) not null,
    goodsIntro varchar(500) not null,
    goodsPrice float not null,
    goodsNum int not null,
    publisher varchar(40) not null,
    photo varchar(40) not null,
    goodsType varchar(40) not null,
    PRIMARY KEY (goodsId)
);



create table orders(
ordersId int auto_increment primary key , 
userId   int , 
orderDate datetime ,
payMode varchar(20) default 'Credit card', 
isPayed bit ,
totalPrice float not null
);

create table orderDetail(
    ordersId int, 
    goodsId int,
    num int not null,
    );

ALTER TABLE `shopping`.`orders`  
  ADD CONSTRAINT `client_id` FOREIGN KEY (`userId`) REFERENCES `shopping`.`users`(`userid`);

ALTER TABLE `shopping`.`orderdetail`  
  ADD CONSTRAINT `order_id` FOREIGN KEY (`ordersId`) REFERENCES `shopping`.`orders`(`ordersId`),
  ADD CONSTRAINT `shangpin_id` FOREIGN KEY (`goodsId`) REFERENCES `shopping`.`goods`(`goodsId`);

INSERT INTO users(username,firstname,lastname, passwd, email,phone,address, grade) VALUES 
("admin","admin","admin", "admin", "admin@dkit.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth",1), 
("shunping","shunping","shunping", "shunping", "shunping@sohu.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth", 1), 
("tester1","tester1","tester1", "tester1", "tester1@sohu.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth", 5), 
("tester2", "tester2","tester2","tester2", "tester2@sohu.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth", 5), 
("tester3", "tester3","tester3","tester3", "tester3@sohu.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth", 5), 
("tester4", "tester4","tester4","tester4", "tester4@sohu.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth", 5), 
("tester5", "tester5","tester5","tester5", "tester5@sohu.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth", 5), 
("tester6", "tester6","tester6","tester6", "tester6@sohu.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth", 5), 
("tester7", "tester7","tester7","tester7", "tester7@sohu.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth", 5), 
("tester8", "tester8","tester8","tester8", "tester8@sohu.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth", 5), 
("tester9", "tester9","tester9","tester9", "tester9@sohu.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth", 5), 
("tester10", "tester10","tester10","tester10", "tester10@sohu.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth", 5), 
("tester11", "tester11","tester11","tester11", "tester11@sohu.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth", 5), 
("tester12", "tester12","tester12","tester12", "tester12@sohu.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth", 5), 
("tester13", "tester13","tester13","tester13", "tester13@sohu.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth", 5), 
("tester14", "tester14","tester14","tester14", "tester14@sohu.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth", 5), 
("tester15", "tester15","tester15","tester15", "tester15@sohu.com","123465789","9 GlenwoodClose Dublin Rd Dundalk louth",5);

insert into goods (goodsName,goodsIntro,goodsPrice,goodsNum,publisher,photo,goodsType)values
("The Hobbit","good movie",49,1,"Warner Bros Pictures","01.jpg","Action Films"),
("The Hunger Games","good movie1",59,1,"Warner Bros Pictures1","02.jpg","Action Films"),
("Thor","good movie2",69,1,"Warner Bros Pictures2","03.jpg","Action Films"),
("Out of the Furnace","good movie3",36,1,"Warner Bros Pictures3","04.jpg","Action Films"),
("The Matrix","good movie4",41,1,"Warner Bros Pictures4","05.jpg","Action Films"),
("Kill Bill","good movie5",55,1,"Warner Bros Pictures5","06.jpg","Action Films"),
("Inception","good movie6",60,1,"Warner Bros Pictures6","07.jpg","Action Films"),
("Sin Cit","good movie7",37,1,"Warner Bros Pictures7","08.jpg","Action Films"),
("The Dark Knight","good movie8",48,1,"Warner Bros Pictures8","09.jpg","Action Films");

