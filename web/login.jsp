<%@page import="java.util.ResourceBundle"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="${language}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>login</title>
    </head>
    <body style="background-color:#CED3FE">
        <%
        ResourceBundle resourceBundle = null;
        resourceBundle = ResourceBundle.getBundle("I18N.text", request.getLocale());
        %>
        <img src="imges/myLogo.png">
        <select id="language" name="language" onchange="submit()">
                <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
                <option value="de" ${language == 'de' ? 'selected' : ''}>German</option>
                <option value="it" ${language == 'it' ? 'selected' : ''}>Italian</option>
        </select>
    
        <center>
            <%
                String err= request.getParameter("err");
                if(err!=null){
                    if(err.equals("1")){
                        out.println("<font color=red size=7>please login！</font><br>");
                    }
                }
            %>
      <!--引入一张图片-->
      
        <hr>
          <%=resourceBundle.getString("USERLOGIN")%>
        <form action="LoginClServlet" method="post">
            <%=resourceBundle.getString("USERNAME")%>：<input type="text" name="username"><br>
            <%=resourceBundle.getString("PASSWORD")%>：<input type="password" name="password"><br>
            <input type="submit" value="login">
            <input type="reset" value="reset">
        </form>
          
        </center>
    <hr>
      <img src="imgs/1.png">
    </body>
</html>

