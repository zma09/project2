<%-- 
    Document   : register.jsp
    Created on : 2014-4-3, 8:46:42
    Author     : Aphasia
--%>





<%@ page language="java" import="java.util.*,com.zm.model.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <style type="text/css">
form {
	width:500px;
	border:1px solid #ccc;
}
fieldset {
	border:0;
	padding:10px;
	margin:10px;
	position:relative;
}
label {
	display:block;
	font:normal 12px/17px verdana;
}
input {width:160px;}
span.hint {
	font:normal 11px/14px verdana;
	background:#eee url(/jscss/demoimg/200910/bg-span-hint-gray.gif) no-repeat top left;
	color:#444;
	border:1px solid #888;
	padding:5px 5px 5px 40px;
	width:250px;
	position:absolute;
	margin: -12px 0 0 14px;
	display:none;
}


fieldset.welldone span.hint {
	background:#9fd680 url(/jscss/demoimg/200910/bg-span-hint-welldone.jpg) no-repeat top left;
	border-color:#749e5c;
	color:#000;
}
fieldset.kindagood span.hint {
	background:#ffffcc url(/jscss/demoimg/200910/bg-span-hint-kindagood.jpg) no-repeat top left;
	border-color:#cc9933;
}


fieldset.welldone {
	background:transparent url(/jscss/demoimg/200910/bg-fieldset-welldone.gif) no-repeat 194px 19px;
}
fieldset.kindagood {
	background:transparent url(/jscss/demoimg/200910/bg-fieldset-kindagood.gif) no-repeat 194px 19px;
}


</style>

<script type="text/javascript">
    <!--
function checkUsernameForLength(whatYouTyped) {
	var fieldset = whatYouTyped.parentNode;
	var txt = whatYouTyped.value;
	if (txt.length > 5) {
		fieldset.className = "welldone";
	}
	else {
		fieldset.className = "";
	}
}
function checkPassword(whatYouTyped) {
	var fieldset = whatYouTyped.parentNode;
	var txt = whatYouTyped.value;
	if (txt.length > 3 && txt.length < 8) {
		fieldset.className = "kindagood";
	} else if (txt.length > 7) {
		fieldset.className = "welldone";
	} else {
		fieldset.className = "";
	}
}
function checkEmail(whatYouTyped) {
	var fieldset = whatYouTyped.parentNode;
	var txt = whatYouTyped.value;
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(txt)) {
		fieldset.className = "welldone";
	} else {
		fieldset.className = "";
	}
}
function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      oldonload();
      func();
    }
  }
}

function prepareInputsForHints() {
  var inputs = document.getElementsByTagName("input");
  for (var i=0; i<inputs.length; i++){
    inputs[i].onfocus = function () {
      this.parentNode.getElementsByTagName("span")[0].style.display = "inline";
    }
    inputs[i].onblur = function () {
      this.parentNode.getElementsByTagName("span")[0].style.display = "none";
    }
  }
}
addLoadEvent(prepareInputsForHints);
 -->
</script>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link rel="stylesheet" type="text/css" href="css/my.css">
        <title>JSP Page</title>
    </head>
    <body  background="imges/bg.jpg" >
        <table width="80%" border="1" align="center">
  <tr>
    <td align="center" class="abc">  
        <jsp:include flush="true" page="head.jsp"></jsp:include>
    </td>
  </tr>
  <tr>
    <td align="center"><table width="100%" border="1">
      <tr>
        <td align="center"><img src="imges/shang.jpg" width="255" height="50" /></td>
      </tr>
      <form action="UsersClServlet?flag=addUser" method="post" type="text"
	name="basicform"
	id="basicform">
      <tr>
        <td align="center"><table width="70%" border="1">
          <tr>
            <td colspan="2" align="center"><span class="abc">Regist and Buyers Info</span></td>
            </tr>
            <fieldset>
          <tr>
            <td width="50%" align="right"><span class="abc" for="username">username：</span></td>
            <td width="50%">              
                    <input type="text"id="username" name="userName" onkeyup="checkUsernameForLength(this);"/>
            <span class="hint">The user name the lowest 6</span></td>
          </tr>
          </fieldset>
          <tr>
            <td align="right"><span class="abc">FirstName:</span></td>
            <td>              <span class="abc">
              <input type="text"id="textfield2" name="firstName" />
            </span></td>
          </tr>
          <tr>
            <td align="right"><span class="abc">LastName:</span></td>
            <td>              <span class="abc">
              <input type="text"id="textfield3" name="lastName" />
            </span></td>
          </tr>
          <fieldset>
           <tr>
            <td align="right"><span class="abc">Password:</span></td>
            <td>              
              <input type="password"
		id="password"
		onkeyup="checkPassword(this);" name="passwd" />
           <span class="hint">The password  the 4~8</span></td>
          </tr>
          </fieldset>
          <tr>
            <td align="right"><span class="abc">Address:</span></td>
            <td>              <span class="abc">
              <input type="text" id="textfield4" name="address"/>
            </span></td>
          </tr>
          <tr>
            <td align="right"><span class="abc">PhoneNumber:</span></td>
            <td>              <span class="abc">
              <input type="text"id="textfield5"  name="phone"/>
            </span></td>
          </tr>
          <fieldset>
          <tr>
            <td align="right"><span class="abc">EmailAddress:</span></td>
            <td>              
              <input type="text"
		id="email"
		onkeyup="checkEmail(this);"  name="email"/>
            <span class="hint">please check!</span></td>
            
          </tr>
          </fieldset>
            <tr>
            <td align="right"><span class="abc">grade:</span></td>
            <td>              <span class="abc">
              <input type="text" id="textfield6"  name="grade"/>
            </span></td>
            
          </tr>
          <tr>
            <td align="right"><span class="abc">
              <input type="submit" name="button" id="button" value="adduser" />
            </span></td>
          
          </tr>
         
        </table></td>
      </tr>
</form>
      <tr>
          <td align="right"><img src="imges/logout.jpg" width="40" height="40" /><a href="index.jsp"><img src="imges/account.jpg" width="40" height="40" /></a></td>
          <%//<a href="OrderClServlet">%>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center">
          <jsp:include flush="true" page="tail.jsp"></jsp:include>
    </td>
  </tr>
</table>
    </body>
</html>

