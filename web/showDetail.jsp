<%-- 
    Document   : showDetail
    Created on : 2013-12-14, 15:16:21
    Author     : Aphasia
--%>
<%@page import="com.zm.model.GoodsBean"%>
<%
 //取出要显示的信息（goodsBean）
    GoodsBean gb=(GoodsBean)request.getAttribute("goodsInfo");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="css/my.css">
<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript">
            //响应返回购物打听好的事件
            function returnHall(){
                //默认open函数式打开一个新页面，如果在后面加入_self即可
                window.open("index.jsp","_self");
                
            }
            //响应点击购买货物的事件
            function addGoods(goodsId){
            //test 是否得到Id
            //window.alert(goodsId);
            window.open("ShoppingClServlet?type=addGoods&goodsId="+goodsId,"_self");
        }
        </script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ShowDetail</title>
    </head>
    <body background="imges/bg.jpg">
        <table width="80%" border="1" align="center">
            <tr>
                <td align="center">
                    <jsp:include flush="true" page="head.jsp"></jsp:include>
                </td>
            </tr>
            <tr>
                <td align="center"><table width="100%" border="1">
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="29%" rowspan="8"><img src="imges/<%=gb.getPhoto() %>" width="120" height="168" /></td>
                            <td width="71%" align="center"><%=gb.getGoodsName() %></td>
                        </tr>
                        <tr>
                            <td>price：<%=gb.getGoodsPrice() %></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>ISBN：<%=gb.getGoodsId() %></td>
                        </tr>
                        <tr>
                            <td>type：<%=gb.getGoodsType() %></td>
                        </tr>
                        <tr>
                            <td>Publishers：<%=gb.getPublisher() %></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td height="96" valign="top"><%=gb.getGoodsIntro() %></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="submit" name="button" onclick="addGoods(<%=gb.getGoodsId() %>)" id="button" value="Buy It" /> 
                                <input type="submit" name="button2" id="button2" onclick="returnHall();" value="Back to the main page" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td align="center">
                    <jsp:include flush="true" page="tail.jsp"></jsp:include>
                </td>
            </tr>
        </table>
    </body>
</html>
